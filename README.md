## Overview
A program which allows you to hide and retrieve an encrypted message in an image. Uses AES-256 bit encryption. The encryption password is optional.

The program will output a PNG image. 

When decrypting an image, it is important to ensure that the image has not been modified in any way, e.g. after transmission over the internet. This includes resizing, or converting to other image formats

## Requirements
Requires Java 6 or higher. Download via the "Downloads" tab