package com.tobi.images.gui;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.tobi.images.encoding.EncryptionUtils;
import com.tobi.images.encoding.PixelUtils;

public class DecodeImagePanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private JTextField fieldInputImage;
	private JButton buttonSelectImage;
	private JPasswordField fieldPassword;
	private JTextArea decodedText;
	private JFileChooser selectImageChooser;
	private JButton buttonGo;
	private Image previewImage = null;
	private JEditorPane previewPane;
	private JLabel labelPreview;

	public DecodeImagePanel() {
		setSize(900, 450);
		setPreferredSize(new Dimension(getWidth(), getHeight()));
		setLayout(null);
		
		previewPane = new JEditorPane() {
			private static final long serialVersionUID = 1L;

			public void paint(Graphics g) {
				if(previewImage == null) {
					labelPreview.setVisible(false);
					return;
				}
				labelPreview.setVisible(true);
				int paneWidth = getWidth();
				int paneHeight = getHeight();
				double scaleFactor = (float) paneWidth / previewImage.getWidth(null);
				int newHeight = (int) (previewImage.getHeight(null) * scaleFactor);
				if(newHeight > paneHeight) scaleFactor = (float) paneHeight / previewImage.getHeight(null);
				int newWidth = (int) (scaleFactor * previewImage.getWidth(null));
				newHeight = (int) (scaleFactor * previewImage.getHeight(null));
				int renderX = (paneWidth / 2) - (newWidth / 2);
				g.drawImage(previewImage, renderX, 0, newWidth, newHeight, null);
			}
		};
		
		addComponents();
		
		selectImageChooser = new JFileChooser();
		File picturesFile = new File(System.getProperty("user.home") + "/Pictures");
		if(picturesFile.exists()) selectImageChooser.setCurrentDirectory(picturesFile);
		selectImageChooser.setDialogTitle("Select Image");
		selectImageChooser.setFileFilter(new FileNameExtensionFilter("Images", "png", "jpg", "jpeg", "bmp"));
		selectImageChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		buttonSelectImage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int option = selectImageChooser.showOpenDialog(null);
				if(option == JFileChooser.APPROVE_OPTION) {
					fieldInputImage.setText(selectImageChooser.getSelectedFile().getAbsolutePath().replace("\\", "/"));
					setPreviewImage(new File(fieldInputImage.getText()));
				}
			}
		});
		buttonGo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				open();
			}
		});
	}
	
	private void open() {
		List<String> errors = new ArrayList<String>();
		String strImageFile = fieldInputImage.getText();
		if(strImageFile == null || strImageFile.trim().isEmpty()) {
			errors.add("Select an image");
		} else {
			File file = new File(strImageFile);
			setPreviewImage(file);
			if(!file.isFile() || previewImage == null) {
				errors.add("Select a valid image");
			}
		}
		char[] password = fieldPassword.getPassword();
		if(password == null || password.length == 0) {
			password = new char[0];
			fieldPassword.setText("");
		}
		if(!errors.isEmpty()) {
			printErrors(errors);
			return;
		}
		decodeText((BufferedImage) previewImage);
	}
	
	private void decodeText(BufferedImage image) {
		List<String> errors = new ArrayList<String>();
		char[] password = fieldPassword.getPassword();
		if(password == null) password = new char[0];
		byte[] bytes = PixelUtils.getData(image);
		try {
			bytes = EncryptionUtils.decryptSalt(bytes, new String(password));
		} catch(Exception e) {
			if(e instanceof ZipException) {
				errors.add("Failed to decompress data: " + e.getMessage() + ".");
				errors.add("Consult the Information tab for help troubleshooting.");
			}
			else if(e instanceof IllegalBlockSizeException || e instanceof BadPaddingException) {
				errors.add("Failed to decrypt the message: " + e.getMessage() + ".");
				errors.add("Ensure the correct password is being used, or consult the Information tab for help troubleshooting.");
			} else {
				errors.add("Failed to decode image: " + e.getMessage() + ".");
				errors.add("Consult the Information tab for help troubleshooting.");
			}
			printErrors(errors);
			return;
		}
		String result = new String(bytes, StandardCharsets.UTF_8);
		decodedText.setText(result);
		JOptionPane.showMessageDialog(null, "Succesfully decoded image.", "Decoded!", JOptionPane.INFORMATION_MESSAGE);
	}
	
	private void setPreviewImage(File file) {
		try {
			Image image = ImageIO.read(file);
			this.previewImage = image;
			previewPane.repaint();
			repaint();
		} catch(Exception e) {
			this.previewImage = null;
		}
	}
	
	private void printErrors(List<String> errors) {
		if(!errors.isEmpty()) {
			String errorsString = "";
			for(String error:errors) {
				errorsString += error + "\n";
			}
			errorsString = errorsString.trim();
			JOptionPane.showMessageDialog(null, errorsString, "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}
	
	private void addComponents() {
		decodedText = new JTextArea();
		decodedText.setLineWrap(true);
		decodedText.setWrapStyleWord(true);
		decodedText.setTabSize(3);
		decodedText.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(decodedText);
		scrollPane.setBounds(337, 36, 553, 403);
		add(scrollPane);
		
		JLabel lblOutputText = new JLabel("Output Text");
		lblOutputText.setBounds(579, 11, 201, 14);
		add(lblOutputText);
		
		JLabel label = new JLabel("Select Image");
		label.setBounds(136, 11, 217, 14);
		add(label);
		
		fieldInputImage = new JTextField();
		fieldInputImage.setEditable(false);
		fieldInputImage.setColumns(10);
		fieldInputImage.setBounds(10, 36, 307, 20);
		add(fieldInputImage);
		
		buttonSelectImage = new JButton("Browse");
		buttonSelectImage.setBounds(100, 67, 127, 23);
		add(buttonSelectImage);
		
		JLabel lblEnterPassword = new JLabel("Enter password");
		lblEnterPassword.setBounds(136, 127, 137, 14);
		add(lblEnterPassword);
		
		fieldPassword = new JPasswordField();
		fieldPassword.setColumns(10);
		fieldPassword.setBounds(100, 152, 137, 20);
		add(fieldPassword);
		
		buttonGo = new JButton("Decode Image");
		buttonGo.setBounds(10, 400, 317, 23);
		add(buttonGo);
		
		labelPreview = new JLabel("Preview");
		labelPreview.setBounds(156, 183, 160, 14);
		add(labelPreview);
		
		previewPane.setEditable(false);
		previewPane.setBounds(10, 208, 317, 181);
		add(previewPane);
	}
}
