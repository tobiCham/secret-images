package com.tobi.images.gui;

import javax.swing.JTextField;
import javax.swing.text.Document;

public class JTextFieldLimit extends JTextField {
	private static final long serialVersionUID = 1L;

	private int maxCharacters;
	private LimitDocument document;

	public JTextFieldLimit(int maxCharacters) {
		this.maxCharacters = maxCharacters;
		if (document == null) document = new LimitDocument(maxCharacters);
		document.setMaxCharacters(maxCharacters);
	}

	public int getMaxCharacters() {
		return maxCharacters;
	}

	@Override
	protected Document createDefaultModel() {
		if (document == null) document = new LimitDocument(maxCharacters);
		return document;
	}
}
