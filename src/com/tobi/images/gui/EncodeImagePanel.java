package com.tobi.images.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.tobi.images.encoding.EncryptionUtils;
import com.tobi.images.encoding.PixelUtils;

public class EncodeImagePanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private JTextField fieldImageFile;
	private JTextField fieldOutputFile;
	private JPasswordField fieldPassword;
	private JButton buttonSelectImage;
	private JButton buttonSelectOutput;
	private JButton buttonGo;
	private JEditorPane previewPane;
	private JTextArea areaText;
	private Image previewImage;
	
	private JFileChooser selectImageChooser;
	private JFileChooser selectOutputChooser;
	private JLabel lblPreview;

	/**
	 * Create the panel.
	 */
	public EncodeImagePanel() {
		setSize(900, 450);
		setPreferredSize(new Dimension(getWidth(), getHeight()));
		setLayout(null);
		
		selectImageChooser = new JFileChooser();
		File picturesFile = new File(System.getProperty("user.home") + "/Pictures");
		if(picturesFile.exists()) selectImageChooser.setCurrentDirectory(picturesFile);
		selectImageChooser.setDialogTitle("Select Image");
		selectImageChooser.setFileFilter(new FileNameExtensionFilter("Images", "png", "jpg", "jpeg", "bmp"));
		selectImageChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		selectOutputChooser = new JFileChooser();
		if(picturesFile.exists()) selectOutputChooser.setCurrentDirectory(picturesFile);
		selectOutputChooser.setDialogTitle("Select output file");
		selectOutputChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		selectOutputChooser.setFileFilter(new FileNameExtensionFilter("Images", "png", "jpg", "jpeg", "bmp"));
		
		previewPane = new JEditorPane() {
			
			private static final long serialVersionUID = 1L;

			public void paint(Graphics g) {
				if(previewImage == null) {
					lblPreview.setVisible(false);
					return;
				}
				lblPreview.setVisible(true);
				int paneWidth = getWidth();
				int paneHeight = getHeight();
				double scaleFactor = (float) paneWidth / previewImage.getWidth(null);
				int newHeight = (int) (previewImage.getHeight(null) * scaleFactor);
				if(newHeight > paneHeight) scaleFactor = (float) paneHeight / previewImage.getHeight(null);
				int newWidth = (int) (scaleFactor * previewImage.getWidth(null));
				newHeight = (int) (scaleFactor * previewImage.getHeight(null));
				int renderX = (paneWidth / 2) - (newWidth / 2);
				g.drawImage(previewImage, renderX, 0, newWidth, newHeight, null);
			}
		};
		previewPane.setEditable(false);
		
		addComponents();
		
		buttonSelectImage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int option = selectImageChooser.showOpenDialog(null);
				if(option != JFileChooser.APPROVE_OPTION) return;
				File file = selectImageChooser.getSelectedFile();
				fieldImageFile.setText(file.getAbsolutePath().replace("\\", "/"));
				setPreviewImage(file);
			}
		});
		
		buttonSelectOutput.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int option = selectOutputChooser.showDialog(null, "Ok");
				if(option != JFileChooser.APPROVE_OPTION) return;
				File file = selectOutputChooser.getSelectedFile();
				fieldOutputFile.setText(file.getAbsolutePath().replace("\\", "/"));
				
				String locationText = fieldOutputFile.getText();
				File outputFile = new File(new File(locationText).getAbsolutePath());
				if(!hasExtension(outputFile)) {
					outputFile = new File(new File(locationText + ".png").getAbsolutePath());
					fieldOutputFile.setText(outputFile.getAbsolutePath().replace("\\", "/"));
				}
			}
		});
		
		buttonGo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
	}
	
	private void save() {
		List<String> errors = new ArrayList<String>();
		
		File inputImageFile = new File(fieldImageFile.getText());
		setPreviewImage(inputImageFile);
		if(!inputImageFile.exists() || previewImage == null) {
			errors.add("Select a valid input image.");
		}
		
		String strOutputFile = fieldOutputFile.getText();
		if(strOutputFile == null || strOutputFile.trim().isEmpty()) {
			errors.add("Select an output file");
		}
		
		char[] password = fieldPassword.getPassword();
		if(password == null) {
			password = new char[0];
			fieldPassword.setText("");
		}
		
		String text = areaText.getText();
		if(text == null || text.trim().isEmpty()) {
			errors.add("Enter text to encode");
		}
		
		if(!errors.isEmpty()) {
			printErrors(errors);
			return;
		}
		export(previewImage);
	}
	
	/**
	 * This method should not be called except in the save() method.
	 */
	private void export(Image saveImage) {
		List<String> errors = new ArrayList<String>();
		String text = areaText.getText();
		String password = new String(fieldPassword.getPassword()).trim();
		byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
		
		try {
			bytes = EncryptionUtils.encryptSalt(bytes, password);
		} catch(Exception e) {
			errors.add("Failed to encode text: " + e.getMessage());
			printErrors(errors);
			return;
		}
		
		int requiredBytes = bytes.length;
		int availableBytes = saveImage.getWidth(null) * saveImage.getHeight(null);
		if(requiredBytes > availableBytes) {
			float requiredWidth = (float) Math.sqrt(((float) requiredBytes * saveImage.getWidth(null)) / (float) saveImage.getHeight(null));
			
			int newWidth = (int) Math.ceil(requiredWidth);
			int newHeight = (int) Math.ceil(requiredBytes / requiredWidth);
			
			errors.add("Image is not big enough to encode the text. Please select a bigger image with size " + newWidth + "x" + newHeight);
			printErrors(errors);
			return;
		}
		
		BufferedImage encodedImage = PixelUtils.addData((BufferedImage) saveImage, bytes);
		String locationText = fieldOutputFile.getText();
		File outputFile = new File(new File(locationText).getAbsolutePath());
		if(!outputFile.getParentFile().exists()) {
			boolean able = outputFile.getParentFile().mkdirs();
			if(!able) {
				errors.add("Failed to create output file directory. No permissions?");
				printErrors(errors);
			}
			return;
		}
		if(outputFile.exists()) {
			int option = JOptionPane.showConfirmDialog(null, "File already exists. Overwrite?", "File Exists", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(option == JOptionPane.CANCEL_OPTION) return;
		}
		try {
			ImageIO.write(encodedImage, "PNG", outputFile);
			String message = "Successfully saved the encoded image to '" + outputFile.getName() + "'!";
			float percentageUsed = (float) requiredBytes / (float) availableBytes;
			percentageUsed *= 100;
			BigDecimal decimal = new BigDecimal(percentageUsed);
			decimal = decimal.setScale(0, RoundingMode.UP);
			message += "\nUsed " + decimal.toString() + "% of the image!"; 
			JOptionPane.showMessageDialog(null, message, "Saved!", JOptionPane.INFORMATION_MESSAGE);
		} catch(Exception e) {
			errors.add("Failed to save image: " + e.getMessage());
			printErrors(errors);
			return;
		}
	}
	
	private static boolean hasExtension(File file)
	{
		String name = file.getName();
		int lastIndex = name.lastIndexOf(".");
		if(lastIndex == -1) return false;
		if(lastIndex + 2 >= name.length()) return false;
		return true;
	}
	
	private void printErrors(List<String> errors) {
		if(!errors.isEmpty()) {
			String errorsString = "";
			for(String error:errors) {
				errorsString += error + "\n";
			}
			errorsString = errorsString.trim();
			JOptionPane.showMessageDialog(null, errorsString, "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}
	
	private void setPreviewImage(File file) {
		try {
			Image image = ImageIO.read(file);
			this.previewImage = image;
			previewPane.repaint();
			repaint();
		} catch(Exception e) {
			this.previewImage = null;
		}
	}
	
	private void addComponents() {
		JLabel lblSelectImage = new JLabel("Select Image");
		lblSelectImage.setBounds(10, 11, 217, 14);
		add(lblSelectImage);
		
		lblPreview = new JLabel("Preview");
		lblPreview.setBounds(360, 160, 40, 14);
		add(lblPreview);
		
		JLabel lblEnterPassword = new JLabel("Enter password");
		lblEnterPassword.setBounds(10, 189, 360, 14);
		add(lblEnterPassword);
		
		JLabel lblChooseOutputLocation = new JLabel("Choose output location");
		lblChooseOutputLocation.setBounds(10, 99, 217, 14);
		add(lblChooseOutputLocation);
		
		buttonGo = new JButton("Encode Image");
		buttonGo.setBounds(629, 403, 261, 23);
		add(buttonGo);
		
		fieldImageFile = new JTextField();
		fieldImageFile.setEditable(false);
		fieldImageFile.setBounds(10, 36, 431, 20);
		add(fieldImageFile);
		fieldImageFile.setColumns(10);
		
		buttonSelectImage = new JButton("Browse");
		buttonSelectImage.setBounds(451, 35, 146, 23);
		add(buttonSelectImage);
		
		fieldOutputFile = new JTextField();
		fieldOutputFile.setEditable(false);
		fieldOutputFile.setBounds(10, 124, 431, 20);
		add(fieldOutputFile);
		fieldOutputFile.setColumns(10);
		
		buttonSelectOutput = new JButton("Select File");
		buttonSelectOutput.setBounds(451, 123, 146, 23);
		add(buttonSelectOutput);
		
		fieldPassword = new JPasswordField();
		fieldPassword.setBounds(10, 214, 137, 20);
		add(fieldPassword);
		fieldPassword.setColumns(10);
		
		previewPane.setBounds(160, 180, 440, 260);
		add(previewPane);
		
		areaText = new JTextArea();
		areaText.setLineWrap(true);
		areaText.setWrapStyleWord(true);
		areaText.setTabSize(3);
		JScrollPane paneText = new JScrollPane(areaText);
		paneText.setBounds(619, 37, 271, 343);
		add(paneText);
		
		JLabel lblEnterText = new JLabel("Enter Text");
		lblEnterText.setBounds(729, 11, 161, 14);
		add(lblEnterText);
	}
}
