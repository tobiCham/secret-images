package com.tobi.images.gui;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class LimitDocument extends PlainDocument {

	private static final long serialVersionUID = 1L;
	private int maxCharacters;

	public LimitDocument(int maxCharacters) {
		this.maxCharacters = maxCharacters;
	}

	@Override
	public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
		if (str == null) return;
		if ((getLength() + str.length()) <= maxCharacters) {
			super.insertString(offset, str, attr);
		}
	}

	public int getMaxCharacters() {
		return maxCharacters;
	}

	public void setMaxCharacters(int maxCharacters) {
		this.maxCharacters = maxCharacters;
	}
}
