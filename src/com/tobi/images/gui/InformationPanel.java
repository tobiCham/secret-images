package com.tobi.images.gui;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class InformationPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	public InformationPanel() {
		setBorder(null);
		setSize(900, 450);
		setPreferredSize(new Dimension(getWidth(), getHeight()));
		setLayout(null);
		
		JLabel labelEncodingIssues = new JLabel("<html><u>Common Issues Encoding:</u></html>");
		labelEncodingIssues.setBounds(10, 203, 274, 14);
		add(labelEncodingIssues);
		labelEncodingIssues.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JTextArea textCommonEncodingIssues = new JTextArea();
		textCommonEncodingIssues.setBorder(null);
		textCommonEncodingIssues.setText("Encoding Data should be relatively straight forward. As long as a valid image is selected, everything shouldwork. If the program fails to save the image, ensure that the program can access the location you are savingit to.\r\n\r\nWhen encoding an image, you can specifiy a password. If nothing is specified, an empty string of text is used.");
		textCommonEncodingIssues.setBounds(10, 228, 880, 78);
		textCommonEncodingIssues.setWrapStyleWord(true);
		textCommonEncodingIssues.setLineWrap(true);
		textCommonEncodingIssues.setEditable(false);
		textCommonEncodingIssues.setBackground(new Color(255, 255, 255, 0));
		textCommonEncodingIssues.setFont(new Font("Tahoma", Font.PLAIN, 13));
		add(textCommonEncodingIssues);
		
		JLabel labelDecodingIssues = new JLabel("<html><u>Common Issues Decoding:</u></html>");
		labelDecodingIssues.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelDecodingIssues.setBounds(10, 336, 274, 14);
		add(labelDecodingIssues);
		
		JTextArea textCommonDecodingIssues = new JTextArea();
		textCommonDecodingIssues.setText("Decoding has more potential to go wrong. Firstly, if the image has been transmitted across the internet it is vital to ensure the image has not been modified while being sent. E.g. it hasn't been re-encoded or hasn't been resized.\r\n\r\nNext, when decoding an image you will also have to specify the password that the image was originally encrypted with. If no encryption was added, leave it blank.");
		textCommonDecodingIssues.setLineWrap(true);
		textCommonDecodingIssues.setWrapStyleWord(true);
		textCommonDecodingIssues.setFont(new Font("Tahoma", Font.PLAIN, 13));
		textCommonDecodingIssues.setEditable(false);
		textCommonDecodingIssues.setBorder(null);
		textCommonDecodingIssues.setBackground(new Color(255, 255, 255, 0));
		textCommonDecodingIssues.setBounds(10, 361, 880, 78);
		add(textCommonDecodingIssues);
		
		JLabel labelExplanation = new JLabel("<html><u>How the program works:</u></html>");
		labelExplanation.setBounds(10, 11, 291, 14);
		labelExplanation.setFont(new Font("Tahoma", Font.PLAIN, 12));
		add(labelExplanation);
		
		JTextArea textExplanation = new JTextArea();
		textExplanation.setBounds(10, 36, 880, 156);
		textExplanation.setText("This program is a program which will add secret messages to images and then get the secret messages back! How cool.\r\n\r\nThe \"Encode\" tab is for adding messages to an image. Select an image to add the message to, select the save file, then enter the secret message you want to add. You can optionally add a password to the message so only you can get that message back! After that, simply click \"Encode Image\" and the image with the message inside should appear wherever you specified. As a side note, the program works best when adding a message to images with lots of variation, for example pictures, and not pixel art.\r\n\r\nThe \"Decode\" tab is for getting that message back! Select the image you want to retrieve the message for, and then specify the password is there is one. Then just click \"Decode Image\" and wait for your message to be retrieved!");
		textExplanation.setLineWrap(true);
		textExplanation.setWrapStyleWord(true);
		textExplanation.setFont(new Font("Tahoma", Font.PLAIN, 13));
		textExplanation.setEditable(false);
		textExplanation.setBorder(null);
		textExplanation.setBackground(new Color(255, 255, 255, 0));
		add(textExplanation);
	}
}
