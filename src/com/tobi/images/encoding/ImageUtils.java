package com.tobi.images.encoding;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class ImageUtils {
	
	public static byte[] getMessage(File imageFile) throws Exception {
		BufferedImage image = ImageIO.read(imageFile);
		byte[] data = PixelUtils.getData(image);
		return data;
	}
	public static void saveMessage(File imageFile, File outputImage, byte[] data) throws Exception {
		BufferedImage image = ImageIO.read(imageFile);
		int pixels = image.getWidth() * image.getHeight();
		int pixelsRequired = data.length * 2;
		if(pixelsRequired > pixels) throw new Exception("Not enough space in the image: " + (pixelsRequired - pixels) + " more pixels needed.");
		image = PixelUtils.addData(image, data);
		ImageIO.write(image, "PNG", outputImage);
	}
}
