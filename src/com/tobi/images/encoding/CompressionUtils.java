package com.tobi.images.encoding;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class CompressionUtils {
	
	public  static byte[] compress(byte[] data) throws Exception {
		ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
		GZIPOutputStream output = new GZIPOutputStream(byteArrayOutput);
		output.write(data);
		output.flush();
		output.close();
		byteArrayOutput.flush();
		byteArrayOutput.close();
		return byteArrayOutput.toByteArray();
	}
	
	public static byte[] decompress(byte[] data) throws Exception {
		ByteArrayInputStream byteArrayInput = new ByteArrayInputStream(data);
		GZIPInputStream input = new GZIPInputStream(byteArrayInput);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		while(true) {
			byte[] uncompressed = new byte[1024];
			int len = input.read(uncompressed);
			if(len < 0) break;
			output.write(uncompressed, 0, len);
		}
		input.close();
		byteArrayInput.close();
		output.close();
		return output.toByteArray();
	}
}
