package com.tobi.images.encoding;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.SecureRandom;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionUtils {
	
	private static final String ALGORITHM = "AES/CBC/PKCS5PADDING";
	private static final String KEY_ALGORITHM = "AES";
	
	private static void removeCryptographyRestrictions() {
	    if (!isRestrictedCryptography()) return;
	    try {
	        final Class<?> jceSecurity = Class.forName("javax.crypto.JceSecurity");
	        final Class<?> cryptoPermissions = Class.forName("javax.crypto.CryptoPermissions");
	        final Class<?> cryptoAllPermission = Class.forName("javax.crypto.CryptoAllPermission");

	        final Field isRestrictedField = jceSecurity.getDeclaredField("isRestricted");
	        isRestrictedField.setAccessible(true);
	        isRestrictedField.set(null, false);

	        final Field defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy");
	        defaultPolicyField.setAccessible(true);
	        final PermissionCollection defaultPolicy = (PermissionCollection) defaultPolicyField.get(null);

	        final Field perms = cryptoPermissions.getDeclaredField("perms");
	        perms.setAccessible(true);
	        ((Map<?, ?>) perms.get(defaultPolicy)).clear();

	        final Field instance = cryptoAllPermission.getDeclaredField("INSTANCE");
	        instance.setAccessible(true);
	        defaultPolicy.add((Permission) instance.get(null));

	    } catch (final Exception e) {
	    	e.printStackTrace();
	    }
	}

	private static boolean isRestrictedCryptography() {
	    // This simply matches the Oracle JRE, but not OpenJDK.
	    return "Java(TM) SE Runtime Environment".equals(System.getProperty("java.runtime.name"));
	}
	
	/**
	 * @param keyBytes <b>Must be 256 bits long, or 32 bytes</b>
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] input, byte[] keyBytes) throws Exception {
		removeCryptographyRestrictions();
		
		SecureRandom rand = new SecureRandom();
		byte[] iv = new byte[16];
		for(int i = 0; i < iv.length; i++) iv[i] = (byte) rand.nextInt(256);
		
		Key key = new SecretKeySpec(keyBytes, KEY_ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
		byte[] encrypted = cipher.doFinal(input);
		byte[] data = new byte[encrypted.length + 16];
		for(int i = 0; i < cipher.getIV().length; i++) data[i] = cipher.getIV()[i];
		for(int i = 0; i < encrypted.length; i++) data[i + 16] = encrypted[i]; 
		return data;
	}
	
	/**
	 * @param keyBytes <b>Must be 256 bits long, or 32 bytes</b>
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] input, byte[] keyBytes) throws Exception {
		removeCryptographyRestrictions();
		byte[] IV = new byte[16];
		byte[] data = new byte[input.length - 16];
		for(int i = 0; i < 16; i++) IV[i] = input[i];
		for(int i = 16; i < input.length; i++) data[i - 16] = input[i];

		Key key = new SecretKeySpec(keyBytes, KEY_ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV));
		return cipher.doFinal(data);
	}
	
	public static byte[] encryptSalt(byte[] data, String passwrd) throws Exception {
		data = CompressionUtils.compress(data);
		byte[] salt = new byte[16];
		SecureRandom rand = new SecureRandom();
		rand.nextBytes(salt);
		
		byte[] password = passwrd.getBytes(StandardCharsets.UTF_8);
		byte[] key = concat(salt, password);
		
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		key = digest.digest(key);
		byte[] encrypted = EncryptionUtils.encrypt(data, key);
		byte[] finalBytes = concat(salt, encrypted);
		return CompressionUtils.compress(finalBytes);
	}
	
	public static byte[] decryptSalt(byte[] data, String passwrd) throws Exception {
		data = CompressionUtils.decompress(data);
		byte[] salt = new byte[16];
        byte[] encrypted = new byte[data.length - salt.length];

        for (int i = 0; i < salt.length; i++) salt[i] = data[i];
        for (int i = 0; i < encrypted.length; i++) encrypted[i] = data[i + salt.length];

        byte[] password = passwrd.getBytes(StandardCharsets.UTF_8);
        byte[] key = concat(salt, password);
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        key = digest.digest(key);

        byte[] decrypted = EncryptionUtils.decrypt(encrypted, key);
        decrypted = CompressionUtils.decompress(decrypted);
        return decrypted;
	}
	
	public static byte[] concat(byte[] a, byte[] b) {
	   int aLen = a.length;
	   int bLen = b.length;
	   byte[] c= new byte[aLen+bLen];
	   System.arraycopy(a, 0, c, 0, aLen);
	   System.arraycopy(b, 0, c, aLen, bLen);
	   return c;
	}
}
