package com.tobi.images.encoding;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class PixelUtils {
	
	public static BufferedImage addData(BufferedImage image, byte[] data) {
		BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g = newImage.getGraphics();
		g.drawImage(image, 0, 0, newImage.getWidth(), newImage.getHeight(), null);
		g.dispose();
		
		int counter = 0;
		
		for(int j = 0; j < newImage.getHeight(); j++) {
			for(int i = 0; i < newImage.getWidth(); i++) {
				if(counter >= data.length) {
					return newImage;
				}
				int pixel = newImage.getRGB(i, j);
				int newPixel = addDataToPixel(pixel, data[counter]);
				newImage.setRGB(i, j, newPixel);
				counter++;
			}
		}
		try {
			throw new Exception("Failed to fill image with data - Not enough room");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newImage;
	}
	
	public static byte[] getData(BufferedImage image) {
		List<Byte> bytes = new ArrayList<Byte>();
		
		for(int i = 0; i < image.getWidth() * image.getHeight(); i++) {
			int x = i % image.getWidth();
			int y = i / image.getWidth();
			byte b = getDataFromPixel(image.getRGB(x, y));
			bytes.add(b);
		}
		byte[] byteArray = new byte[bytes.size()];
		for(int i = 0; i < bytes.size(); i++) byteArray[i] = bytes.get(i);
		return byteArray;
	}
	
	private static int getPixel(int red, int green, int blue, int alpha) {
		red = red & 255;
		green = green & 255;
		blue = blue & 255;
		alpha = alpha & 255;
		return (alpha << 24) | (red << 16) | (green << 8)  | (blue << 0);
	}
	
	private static byte encodeLastTwo(byte value, byte data) {
		value = (byte) (value & 252);
		data = (byte) (data & 3);
		return (byte) (value | data);
	}
	
	/**
	 * @return Two bits directly after the starting position. 0 = 8, 1 = 6, 2 = 4, 3 = 2. Any other values except 0, 1, 2 or 3 will return the initial value.
	 */
	private static byte getTwoBits(byte value, int index) {
		if(index == 0) return (byte) ((value >> 6) & 3);
		if(index == 1) return (byte) ((value >> 4) & 3);
		if(index == 2) return (byte) ((value >> 2) & 3);
		if(index == 3) return (byte) ((value) & 3);
		return value;
	}
	
	/**
	 * @return An 8 bit byte with each parameter being two bits. E.g. 10 + 11 + 00 + 01 = 10110001
	 */
	private static byte makeByte(byte first, byte second, byte third, byte last) {
		byte output = 0;
		output = (byte) ((output | ((first << 6))));
		output = (byte) ((output | ((second << 4))));
		output = (byte) ((output | ((third << 2))));
		output = (byte) ((output | ((last << 0))));
		return output;
	}
	
	private static int addDataToPixel(int pixel, byte data) {
		byte alpha = (byte) ((pixel >> 24) & 255);
		byte red = (byte) ((pixel >> 16) & 255);
		byte green = (byte) ((pixel >> 8) & 255);
		byte blue = (byte) (pixel & 255);
		
		byte first = getTwoBits(data, 0);
		byte second = getTwoBits(data, 1);
		byte third = getTwoBits(data, 2);
		byte last = getTwoBits(data, 3);
		
		byte newAlpha = encodeLastTwo(alpha, first);
		byte newRed = encodeLastTwo(red, second);
		byte newGreen = encodeLastTwo(green, third);
		byte newBlue = encodeLastTwo(blue, last);
		
		int newPixel = getPixel(newRed, newGreen, newBlue, newAlpha);
		return newPixel;
	}
	
	private static byte getDataFromPixel(int pixel) {
		byte alpha = (byte) ((pixel >> 24) & 255);
		byte red = (byte) ((pixel >> 16) & 255);
		byte green = (byte) ((pixel >> 8) & 255);
		byte blue = (byte) (pixel & 255);
		
		byte first = getTwoBits(alpha, 3);
		byte second = getTwoBits(red, 3);
		byte third = getTwoBits(green, 3);
		byte last = getTwoBits(blue, 3);
		return makeByte(first, second, third, last);
	}
}
