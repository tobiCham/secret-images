package com.tobi.images;

import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

import com.tobi.images.gui.DecodeImagePanel;
import com.tobi.images.gui.EncodeImagePanel;
import com.tobi.images.gui.InformationPanel;

public class Main {
	
	public static void main(String[] args) throws Exception {
		try {
			initStyle();
		} catch(Exception e) {
			e.printStackTrace();
		}
		JFrame frame = new JFrame();
		addIcon(frame);
		frame.setSize(900, 160);
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.SCROLL_TAB_LAYOUT);
		tabbedPane.add("Encode", new EncodeImagePanel());
		tabbedPane.add("Decode", new DecodeImagePanel());
		tabbedPane.add("Help and Information", new InformationPanel());
		frame.add(tabbedPane);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
	}
	
	private static void initStyle() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {}
	}
	
	private static void addIcon(JFrame frame) {
		try {
			Image icon = ImageIO.read(Main.class.getResource("/icon.png"));
			frame.setIconImage(icon);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
